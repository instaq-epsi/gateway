import { config } from "./config";
import { ApolloServer } from "apollo-server-fastify";
import fastify from "fastify";
import { ApolloGateway } from "@apollo/gateway";
const gateway = new ApolloGateway(config.services);
const server = new ApolloServer({
  gateway,

  subscriptions: false
});

const app = fastify();

(async () => {
  app.register(server.createHandler());
  const address = await app.listen(config.port, config.host);
  console.log(`Gateway started on ${address}`);
})();
